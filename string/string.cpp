﻿#include "pch.h"
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <string>

const int NumberOfLabels = 7, OpeningX = 3, OpeningY = 1, SizeOfText = 255, RandomConst = 60;
int ItemOfMenu;
int NubersOfSentence;
int StartOfSentence[RandomConst];
int WordsInSentence[SizeOfText], WordsInText, NumberOfWord;
char Text[SizeOfText], Sentence[RandomConst][SizeOfText],Words[RandomConst][SizeOfText];
char LabelOfMenu;
bool SentenceWithTheGivenWord[SizeOfText];
char Menu[NumberOfLabels][SizeOfText] = { "Enter text",
		"Selection of each word and number of words",
		"The number of words in the second sentence and the output of all odd words",
		"The first number of the second sentence in binary notation",
		"The number from the first sentence into a Roman numeral",
		"Search for sentences with a given word, the number of odd words and words with letters in alphabetical order",
		"Exit" };

HANDLE hConsole, hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
using namespace std;

void GotoXY(int goX, int goY);
void SetColor(int text, int background);
void WriteMenuToScreen(int menuX, int menuY);
void WriteSelectParagraph(int secectX, int secectY);
void WriteParagraphNormal(int normalX, int normalY);
void InputString();
void OutputEachWord();
void NumberOfWordsInText();
void NumberOfWordsIn2Sentence();
void OutputOddNumberedWordInText();
void DecimalToBinary();
void FirstNumberToRoman();
void OutputOfSentenceWithTheGivenWord();
void OutputOddNumberedWordInSentence();
void WordsWithAlphabeticallyOrderedLetters();

void main() {
	system("cls");
	ItemOfMenu = 0;
	WriteMenuToScreen(OpeningX, OpeningY);
	WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
	do {
		LabelOfMenu = _getch();
		if (LabelOfMenu != 13) {
			LabelOfMenu = _getch();
			switch (LabelOfMenu)
			{
			case 80:
				if (ItemOfMenu < NumberOfLabels - 1) {
					WriteParagraphNormal(OpeningX, OpeningY + ItemOfMenu);
					ItemOfMenu++;
					WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
				}
				break;
			case 72:
				if (ItemOfMenu > 0) {
					WriteParagraphNormal(OpeningX, OpeningY + ItemOfMenu);
					ItemOfMenu--;
					WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
				}
				break;
			}
		}
		else {
			if (LabelOfMenu == 13) {
				switch (ItemOfMenu) {
				case 0:
					system("cls");
					SetColor(7, 0);
					InputString();
					system("pause");
					break;
				case 1:
					system("cls");
					SetColor(7, 0);
					OutputEachWord();
					NumberOfWordsInText();
					system("pause");
					break;
				case 2:
					system("cls");
					SetColor(7, 0);
					NumberOfWordsIn2Sentence();
					OutputOddNumberedWordInText();
					system("pause");
					break;
				case 3:
					system("cls");
					SetColor(7, 0);
					DecimalToBinary();
					system("pause");
					break;
				case 4:
					system("cls");
					SetColor(7, 0);
					FirstNumberToRoman();
					system("pause");
					break;
				case 5:
					system("cls");
					SetColor(7, 0);
					OutputOfSentenceWithTheGivenWord();
					WordsWithAlphabeticallyOrderedLetters();
					system("pause");
					break;
				case 6:
					LabelOfMenu = 27;
					break;
				}
			}
			WriteMenuToScreen(OpeningX, OpeningY);
			WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
		}
	} while (LabelOfMenu != 27);
}
void GotoXY(int goX, int goY) {
	COORD coord = { goX, goY };
	SetConsoleCursorPosition(hStdOut, coord);
}
void SetColor(int text, int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}
void WriteMenuToScreen(int menuX, int menuY) {
	int i0;
	system("cls");
	SetColor(7, 0);
	for (i0 = 0; i0 != NumberOfLabels; i0++) {
		GotoXY(menuX, menuY + i0);
		cout << Menu[i0];
	}
}
void WriteSelectParagraph(int secectX, int secectY) {
	SetColor(2, 0);
	GotoXY(secectX, secectY);
	cout << Menu[ItemOfMenu];
}
void WriteParagraphNormal(int normalX, int normalY) {
	SetColor(7, 0);
	GotoXY(normalX, normalY);
	cout << Menu[ItemOfMenu];
}
void InputString()
{
	for (int i = 0; i < SizeOfText; i++) {
		Text[i] = NULL;
		WordsInSentence[i] = 0;
	}
	cout << "Enter the text(not more than " << SizeOfText << " symblos):\n";
	NubersOfSentence = 0;
	StartOfSentence[1] = 0;
	NumberOfWord = 0;
	cin.getline(Text, SizeOfText);
	Text[SizeOfText - 2] = '.';
	Text[SizeOfText - 1] = ' ';
	int k = 0;
	for (int i = 0; i < SizeOfText; i++) {
		if ((Text[i] == '.' || Text[i] == '!' || Text[i] == '?') && Text[i + 1] == ' ') {
			NubersOfSentence++;
			StartOfSentence[NubersOfSentence + 1] = i + 2;
		}
		if (Text[i] != '.' && Text[i] != '!' && Text[i] != '?' && Text[i] != ' ') {
			Words[NumberOfWord][k] = Text[i];
			k++;
		}
		else  if (Text[i+1] != ' '){
			NumberOfWord++;
			k = 0;
		}
		

	}
	for (int i = 1; i <= NubersOfSentence; i++) {
		WordsInSentence[i] = 0;
		for (int j = StartOfSentence[i]; j < StartOfSentence[i + 1] - 2; j++) {
			if ((Text[j] == ' ') && (Text[j - 1] != ' ')) {
				WordsInSentence[i]++;
			}
		}
		WordsInSentence[i]++;
	}
	for (int k = 1; k <= NubersOfSentence; k++) {
		int j = 0;
		for (int i = StartOfSentence[k]; i <= StartOfSentence[k + 1] - 2; i++) {
			Sentence[k][j] = Text[i];
			j++;
		}
	}
}
void OutputEachWord()
{
	char symbol = ' ';
	for (int i = 0; i < NumberOfWord; i++) {
		system("pause");
		for (int j = 0; Words[i][j] != '\0'; j++) {
			cout << Words[i][j];
			if (Words[i][j+1] == '\0') {
				cout << '\n';
				continue;
			}
		}
	}
}
void NumberOfWordsInText()
{
	WordsInText = 0;
	for (int i = 1; i <= NubersOfSentence; i++) {
		WordsInText += WordsInSentence[i];
	}
	cout << "The text contains " << WordsInText << " words.\n";
}
void NumberOfWordsIn2Sentence()
{
	cout << "The second sentence contains " << WordsInSentence[2] << " words.\n";
}
void OutputOddNumberedWordInText()
{
	bool oddWord = 1;
	for (int i = 0; i < SizeOfText; i++) {
		if (Text[i] == ' ') {
			if (oddWord) {
				cout << '\n';
			}
			oddWord = !(oddWord);
		}
		else if ((Text[i] != '.') && oddWord) {
			cout << Text[i];
		}
	}
}
void DecimalToBinary()
{
	int j = 0;
	for (int i = StartOfSentence[2]; i < StartOfSentence[3]; i++) {
		Sentence[2][j] = Text[i];
		j++;
	}
	char *DecInChar = strpbrk(Sentence[2], "-0123456789");
	char *DecFlInChar = strpbrk(Sentence[2], "-0123456789.");
	if (DecInChar != NULL) {
		if (atoi(DecInChar) == stof(DecFlInChar)) {
			cout << "First integer number in 2 sentence in binary notation: ";
			int Dec = atoi(DecInChar);
			int Bin[RandomConst];
			int i = 0;
			while (Dec != 0)
			{
				Bin[i] = Dec % 2;
				Dec = Dec / 2;
				i++;
			}
			j = i;
			for (i = j - 1; i >= 0; i--)
				cout << Bin[i];
			cout << '\n';
		}
		else {
			cout << "No number in 2-nd sentence.\n";
		}
	}
	else {
		cout << "No number in 2-nd sentence.\n";
	}
}
void FirstNumberToRoman()
{
	int j = 0;
	for (int i = StartOfSentence[1]; i < StartOfSentence[2]; i++) {
		Sentence[1][j] = Text[i];
		j++;
	}
	char *DecInChar = strpbrk(Sentence[1], "0123456789");
	if (DecInChar != NULL) {
		int Dec = atoi(DecInChar);
		int DecNumber[4];
		int i;
		cout << "Number in 1 sentence in roman notation: ";
		DecNumber[3] = Dec % 10;
		DecNumber[2] = (Dec / 10) % 10;
		DecNumber[1] = (Dec / 100) % 10;
		DecNumber[0] = (Dec / 1000) % 10;
		for (i = 0; i < 1; i++) {
			if (DecNumber[i] > 0) {
				if (DecNumber[i] == 1) {
					cout << "M";
				}
			}
		}
		for (i = 1; i < 2; i++) {
			if (DecNumber[i] > 0) {
				if (DecNumber[i] == 1) {
					cout << "C";
				}
				if (DecNumber[i] == 2) {
					cout << "CC";
				}
				if (DecNumber[i] == 3) {
					cout << "CCC";
				}
				if (DecNumber[i] == 4) {
					cout << "CCCC";
				}
				if (DecNumber[i] == 5) {
					cout << "D";
				}
				if (DecNumber[i] == 6) {
					cout << "DC";
				}
				if (DecNumber[i] == 7) {
					cout << "DCC";
				}
				if (DecNumber[i] == 8) {
					cout << "DCCC";
				}
				if (DecNumber[i] == 9) {
					cout << "CM";
				}
			}
		}
		for (i = 2; i < 3; i++) {
			if (DecNumber[i] > 0) {
				if (DecNumber[i] == 1) {
					cout << "X";
				}
				if (DecNumber[i] == 2) {
					cout << "XX";
				}
				if (DecNumber[i] == 3) {
					cout << "XXX";
				}
				if (DecNumber[i] == 4) {
					cout << "XXXX";
				}
				if (DecNumber[i] == 5) {
					cout << "L";
				}
				if (DecNumber[i] == 6) {
					cout << "LX";
				}
				if (DecNumber[i] == 7) {
					cout << "LXX";
				}
				if (DecNumber[i] == 8) {
					cout << "LXXX";
				}
				if (DecNumber[i] == 9) {
					cout << "XC";
				}
			}
		}
		for (i = 3; i < 4; i++) {
			if (DecNumber[i] > 0) {
				if (DecNumber[i] == 1) {
					cout << "I";
				}
				if (DecNumber[i] == 2) {
					cout << "II";
				}
				if (DecNumber[i] == 3) {
					cout << "III";
				}
				if (DecNumber[i] == 4) {
					cout << "IV";
				}
				if (DecNumber[i] == 5) {
					cout << "V";
				}
				if (DecNumber[i] == 6) {
					cout << "VI";
				}
				if (DecNumber[i] == 7) {
					cout << "VII";
				}
				if (DecNumber[i] == 8) {
					cout << "VIII";
				}
				if (DecNumber[i] == 9) {
					cout << "IX";
				}
			}
		}
	}
	else {
		cout << "No number in 1-st sentence.";
	}
	cout << '\n';
}
void OutputOfSentenceWithTheGivenWord()
{
	char InputWord[RandomConst];
	cout << "Enter search term:\n";
	cin >> InputWord;
	cout << "Centences with term:\n";
	for (int i = 1; i <= NubersOfSentence; i++) {
		if (strstr(Sentence[i], InputWord) != NULL) {
			SentenceWithTheGivenWord[i] = 1;
		}
		else {
			SentenceWithTheGivenWord[i] = 0;
		}
		if (SentenceWithTheGivenWord[i] == 1) {
			cout << Sentence[i] << "\n";
		}
	}
}

void WordsWithAlphabeticallyOrderedLetters()
{
	int CountOddWord = 0;
	cout << "Words where the letter is alphabetically :\n";
	int k = 0, i1 = 0;
	for (int i = 1; i <= NubersOfSentence; i++) {
		k = 0;
		if (SentenceWithTheGivenWord[i] == 1) {
			for (int j = 1; j <= WordsInSentence[i]; j++) {
				i1 = 0;
				if (j % 2 == 1) {
					CountOddWord++;
				}
				char SortingWord[RandomConst] = "";
				while (Sentence[i][k] != ' ' && Sentence[i][k] != ',' && Sentence[i][k] != '.' && Sentence[i][k] != '?' && Sentence[i][k] != '!' && Sentence[i][k] != ';' && Sentence[i][k] != ':') {
					SortingWord[i1] = Sentence[i][k];
					k++;
					i1++;
				}
				if (SortingWord[0] < SortingWord[1] && SortingWord[1] < SortingWord[2]) {
					cout << SortingWord << "\n";
				}
				k++;
			}
		}
	}
	cout << "Number of odd word: " << CountOddWord << '\n';
}